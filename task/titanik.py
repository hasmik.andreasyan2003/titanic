import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    median ={}
    titles = ["Mr.", "Mrs.", "Miss."]
    for i in titles:
      title_df = df[df['Name'].str.contains(i)]
      median_age = round(title_df['Age'].median())
      miss_age = title_df['Age'].isna().sum()
      median[i]=(i,miss_age,median_age)
    res = list(median.values()) 
    return res